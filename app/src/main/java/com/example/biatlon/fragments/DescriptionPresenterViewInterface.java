package com.example.biatlon.fragments;

import com.example.biatlon.data.Discipline;

import java.io.DataInput;
import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface DescriptionPresenterViewInterface extends MvpView {
    @AddToEnd
    void showDisciplines(List<Discipline> data);
}
