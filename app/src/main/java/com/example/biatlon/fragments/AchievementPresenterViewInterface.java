package com.example.biatlon.fragments;

import com.example.biatlon.data.Achievement;
import com.example.biatlon.data.Discipline;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface AchievementPresenterViewInterface extends MvpView {
    @AddToEnd
    void showAchievements(List<Achievement> data);
}
