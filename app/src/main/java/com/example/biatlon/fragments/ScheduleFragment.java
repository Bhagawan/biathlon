package com.example.biatlon.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.biatlon.R;
import com.example.biatlon.data.Schedule;
import com.example.biatlon.util.MyServerClient;
import com.example.biatlon.util.ScheduleAdapter;
import com.example.biatlon.util.ServerClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleFragment extends Fragment {
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_schedule_list, container, false);

        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<Schedule>> call = client.sendRequest("schedule.json");
        call.enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(@NonNull Call<List<Schedule>> call, @NonNull Response<List<Schedule>> response) {
                RecyclerView recyclerView = view.findViewById(R.id.schedule_recycler);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(new ScheduleAdapter(response.body()));
            }

            @Override
            public void onFailure(@NonNull Call<List<Schedule>> call, @NonNull Throwable t) {
                Toast.makeText(getContext(),"Error!", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}