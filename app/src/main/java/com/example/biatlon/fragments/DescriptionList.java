package com.example.biatlon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biatlon.R;
import com.example.biatlon.data.Discipline;
import com.example.biatlon.util.DescriptionAdapter;

import java.util.List;

public class DescriptionList extends Fragment {
    private View view;
    private List<Discipline> list;

    public DescriptionList(List<Discipline> list) {
        this.list = list;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.description_list, container, false);
        if (list != null) {
            RecyclerView recyclerView = view.findViewById(R.id.description_recycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            DescriptionAdapter adapter = new DescriptionAdapter(list);
            adapter.setClickListener((view, position) -> {
                InfoFragment infoFragment = new InfoFragment(list.get(position));
                FragmentTransaction ft = getParentFragmentManager().beginTransaction();
                ft.replace(R.id.description_info_fragmentView, infoFragment).addToBackStack(null).commit();
            });
            recyclerView.setAdapter(adapter);

        }
        return view;
    }
}