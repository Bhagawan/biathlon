package com.example.biatlon.fragments;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biatlon.R;
import com.example.biatlon.data.Discipline;
import com.example.biatlon.data.Schedule;
import com.example.biatlon.util.MyServerClient;
import com.example.biatlon.util.ScheduleAdapter;
import com.example.biatlon.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class DescriptionPresenter extends MvpPresenter<DescriptionPresenterViewInterface>{

    @Override
    public void onFirstViewAttach() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<Discipline>> call = client.downloadDescription("description.json");
        call.enqueue(new Callback<List<Discipline>>() {
            @Override
            public void onResponse(@NonNull Call<List<Discipline>> call, @NonNull Response<List<Discipline>> response) {
                getViewState().showDisciplines(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Discipline>> call, @NonNull Throwable t) {

            }
        });

    }


}
