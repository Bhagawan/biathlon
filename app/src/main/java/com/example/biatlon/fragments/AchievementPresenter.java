package com.example.biatlon.fragments;

import androidx.annotation.NonNull;

import com.example.biatlon.data.Achievement;
import com.example.biatlon.data.Discipline;
import com.example.biatlon.util.MyServerClient;
import com.example.biatlon.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class AchievementPresenter extends MvpPresenter<AchievementPresenterViewInterface> {

    @Override
    public void onFirstViewAttach() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<Achievement>> call = client.downloadAchievements("achievement.json");
        call.enqueue(new Callback<List<Achievement>>() {
            @Override
            public void onResponse(@NonNull Call<List<Achievement>> call, @NonNull Response<List<Achievement>> response) {
                getViewState().showAchievements(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Achievement>> call, @NonNull Throwable t) {

            }
        });

    }
}
