package com.example.biatlon.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.biatlon.R;
import com.example.biatlon.data.Achievement;
import com.example.biatlon.util.AchievementAdapter;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class AchievementFragment extends MvpAppCompatFragment implements AchievementPresenterViewInterface {
    private View view;


    @InjectPresenter
    AchievementPresenter achievementPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_achievement, container, false);

        return view;
    }

    @Override
    public void showAchievements(List<Achievement> data) {
        RecyclerView recyclerView = view.findViewById(R.id.achievement_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new AchievementAdapter(data));
    }
}