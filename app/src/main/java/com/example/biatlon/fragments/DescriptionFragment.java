package com.example.biatlon.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.biatlon.R;
import com.example.biatlon.data.Discipline;
import com.example.biatlon.util.DescriptionAdapter;
import com.example.biatlon.util.ScheduleAdapter;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class DescriptionFragment extends MvpAppCompatFragment implements DescriptionPresenterViewInterface {
    private View view;

    @InjectPresenter
    DescriptionPresenter descriptionPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_description, container, false);

        return view;
    }

    @Override
    public void showDisciplines(List<Discipline> data) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.description_info_fragmentView, new DescriptionList(data)).commit();
    }
}