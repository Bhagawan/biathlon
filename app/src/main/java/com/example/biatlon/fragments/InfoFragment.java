package com.example.biatlon.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.biatlon.R;
import com.example.biatlon.data.Discipline;

public class InfoFragment extends Fragment {
    private View view;
    private Discipline discipline;

    public InfoFragment(Discipline discipline) {
        this.discipline = discipline;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_info, container, false);
        if(discipline != null) {
            TextView name = view.findViewById(R.id.info_name);
            name.setText(discipline.name);
            TextView text = view.findViewById(R.id.info_text);
            text.setMovementMethod(new ScrollingMovementMethod());
            text.setText(discipline.text);
        }
        ImageButton backButton = view.findViewById(R.id.info_back_button);
        backButton.setOnClickListener(v->{
            //FragmentTransaction ft = getParentFragmentManager().beginTransaction();
            getParentFragmentManager().popBackStack();
            //ft.remove(this).commit();
        });
        return view;
    }
}