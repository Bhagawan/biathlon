package com.example.biatlon.data;

import com.google.gson.annotations.SerializedName;

public class Achievement {
    @SerializedName("name")
    public String name;
    @SerializedName("img")
    public String img;
    @SerializedName("date")
    public String date;
    @SerializedName("country")
    public String country;
    @SerializedName("achievements")
    public String[] achievements;
}
