package com.example.biatlon.data;

import com.google.gson.annotations.SerializedName;

public class Schedule {
    @SerializedName("name")
    public String name;
    @SerializedName("date")
    public String date;
    @SerializedName("flag")
    public String flag;
    @SerializedName("races")
    public Race[] races;

    public class Race {
        @SerializedName("name")
        public String name;
        @SerializedName("time")
        public String time;
    }

}
