package com.example.biatlon.data;

import com.google.gson.annotations.SerializedName;

public class SplashResponse {
    @SerializedName("url")
    public String url;
}
