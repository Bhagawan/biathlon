package com.example.biatlon.data;

import com.google.gson.annotations.SerializedName;

public class Discipline {
    @SerializedName("name")
    public String name;
    @SerializedName("text")
    public String text;
}
