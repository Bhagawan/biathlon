package com.example.biatlon.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biatlon.R;
import com.example.biatlon.data.Discipline;

import java.util.List;

public class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.ViewHolder>{
    private List<Discipline> data;
    private ItemClickListener mClickListener;

    public DescriptionAdapter(List<Discipline> data) {
        this.data = data;
    }

     public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.description_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAbsoluteAdapterPosition());
        }
    }

    @NonNull
    @Override
    public DescriptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.discipline_name, parent, false);
        return new DescriptionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DescriptionAdapter.ViewHolder holder, int position) {
        holder.name.setText(data.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(data==null) return 0;
        else return data.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
