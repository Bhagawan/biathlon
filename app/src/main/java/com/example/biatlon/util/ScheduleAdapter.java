package com.example.biatlon.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biatlon.R;
import com.example.biatlon.data.Schedule;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int STAGE_VIEW = 1;
    private static final int ITEM_VIEW = 2;

    private List<Schedule> data;
    private ArrayList<Integer> expanded = new ArrayList<>();

    public ScheduleAdapter(List<Schedule> data) {
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView stageName;
        private TextView stageDate;
        private ImageView stageFlag;
        private LinearLayout stageLayout;

        private TextView itemName;
        private TextView itemTime;
        public LinearLayout itemLayout;

        public ViewHolder(final View itemView) {
            super(itemView);

            stageName = itemView.findViewById(R.id.schedule_header_name);
            stageDate = itemView.findViewById(R.id.schedule_header_date);
            stageFlag = itemView.findViewById(R.id.schedule_header_flag);
            stageLayout = itemView.findViewById(R.id.schedule_stage);

            itemName = itemView.findViewById(R.id.schedule_item_name);
            itemTime = itemView.findViewById(R.id.schedule_item_time);
            itemLayout = itemView.findViewById(R.id.schedule_item);
        }

        public void bindViewStage(int pos) {
            int n = getStage(pos);
            stageName.setText(data.get(n).name);
            stageDate.setText(data.get(n).date);
            Picasso.get().load(data.get(n).flag).resize(50,40).into(stageFlag);
            itemView.setOnClickListener(v-> {
                toggle(pos);
            });
        }

        public void bindViewItem(int pos) {
            int s = getStage(pos);
            int n = getNumberOfRace(pos);
            if(n >= 0) {
                itemName.setText(data.get(s).races[n].name);
                itemTime.setText(data.get(s).races[n].time);
            }

            if(isExpanded(pos)) itemLayout.setVisibility(View.VISIBLE);
            else itemLayout.setVisibility(View.GONE);

        }

    }

    public class StageViewHolder extends ViewHolder {
        public StageViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class ItemViewHolder extends ViewHolder {
        public ItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        if (viewType == STAGE_VIEW) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_stage, parent, false);
            return new StageViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item, parent, false);
            return new ItemViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof StageViewHolder) {
                StageViewHolder vh = (StageViewHolder) holder;
                vh.bindViewStage(position);
            } else if (holder instanceof ItemViewHolder) {
                ItemViewHolder vh = (ItemViewHolder) holder;
                vh.bindViewItem(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if(data == null) return 0;
        if(data.isEmpty()) return 0;
        else {
            int c = 0;
            for(int i = 0; i < data.size(); i++) {
                c++;
                for(int j = 0; j < data.get(i).races.length; j++) {
                    c++;
                }
            }
            return c;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(getNumberOfRace(position) >= 0) return ITEM_VIEW;
        else return STAGE_VIEW;
    }

    private int getStage(int pos) {
        for(int i = 0; i < data.size(); i++) {
            pos--;
            if(pos < 0) return i;
            for(int j = 0; j < data.get(i).races.length; j++) {
                pos--;
                if(pos < 0) return i;
            }
        }
        return 0;
    }

    private int getNumberOfRace(int pos) {
        int num = -1;
        for(int i = 0; i < data.size(); i++) {
            pos--;
            num = -1;
            if(pos < 0) return num;
            for(int j = 0; j < data.get(i).races.length; j++) {
                num++;
                pos--;
                if(pos < 0) return num;
            }
        }
        return num;
    }
    private boolean isExpanded(int pos) {
        if(expanded.isEmpty()) return false;
        else for(Integer integer:expanded) {
            if(integer == pos) return true;
        }
        return false;
    }

    private void toggle(int pos) {
        if(isExpanded(pos)) {
            for(int i = 0; i < data.get(getStage(pos)).races.length + 1 ; i++) {
                if(!expanded.isEmpty())
                    expanded.remove(expanded.indexOf(pos+i));
            }
        } else {
            for(int i = 0; i < data.get(getStage(pos)).races.length + 1 ; i++) {
                expanded.add(pos + i);
            }
        }
        for(int i = 0; i < data.get(getStage(pos)).races.length + 1 ; i++) {
            notifyItemChanged(pos + i);
        }
    }
}