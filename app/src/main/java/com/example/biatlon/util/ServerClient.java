package com.example.biatlon.util;

import com.example.biatlon.data.Achievement;
import com.example.biatlon.data.Discipline;
import com.example.biatlon.data.Schedule;
import com.example.biatlon.data.SplashResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServerClient {

    @FormUrlEncoded
    @POST("Biathlon/{file}")
    Call<SplashResponse> getSplash(@Path("file") String file, @Field("locale") String locale);

    @GET("Biathlon/{file}")
    Call<List<Schedule>> sendRequest(@Path("file") String file);

    @GET("Biathlon/{file}")
    Call<List<Discipline>> downloadDescription(@Path("file") String file);

    @GET("Biathlon/{file}")
    Call<List<Achievement>> downloadAchievements(@Path("file") String file);
}
