package com.example.biatlon.util;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.style.BulletSpan;
import android.text.style.LeadingMarginSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biatlon.R;
import com.example.biatlon.data.Achievement;
import com.example.biatlon.data.Discipline;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.ViewHolder> {
    private List<Achievement> data;

    public AchievementAdapter(List<Achievement> data) {
        this.data = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView birthday;
        private TextView country;
        private ImageView face;
        private TextView achievements;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.achievement_card_name);
            birthday = itemView.findViewById(R.id.achievement_card_birthday);
            country = itemView.findViewById(R.id.achievement_card_country);
            achievements = itemView.findViewById(R.id.achievement_card_achievements);
            face = itemView.findViewById(R.id.achievement_card_face);
        }
    }

    @NonNull
    @Override
    public AchievementAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.achievement_card, parent, false);
        return new AchievementAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AchievementAdapter.ViewHolder holder, int position) {
        Achievement achievement = data.get(position);
        holder.name.setText(achievement.name);
        holder.birthday.setText(achievement.date);
        holder.country.setText(achievement.country);
        Picasso.get().load(achievement.img).resize(100,100).into(holder.face);

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();

        int f;
        int e;
        for(String string: achievement.achievements) {
            f = stringBuilder.length();
            stringBuilder.append(string).append("\n");
            e = stringBuilder.length();

            stringBuilder.setSpan(new BulletSpan(10), f, e, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        holder.achievements.setText(stringBuilder);
    }

    @Override
    public int getItemCount() {
        if(data==null) return 0;
        else return data.size();
    }
}
